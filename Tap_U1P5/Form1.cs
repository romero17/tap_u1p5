﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tap_U1P5
{
    public partial class Form1 : Form
    {
        private Double operar;
        private String operador;

        private String punto;
        public Form1()
        {
            operar = 0.0;
            operador = "";
            InitializeComponent();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void buttonIgual_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            String valor = btn.Text;
            try
            {
                int.Parse(btn.Text);
                boxDisplay.Text += valor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                switch (valor)
                {
                    case "+":
                        operar += Double.Parse(boxDisplay.Text);
                        boxDisplay.Clear();
                        operador = "suma";
                        break;
                    case "-":
                        operar += Double.Parse(boxDisplay.Text);
                        boxDisplay.Clear();
                        operador = "resta";
                        break;
                    case "x":
                        operar += Double.Parse(boxDisplay.Text);
                        boxDisplay.Clear();
                        operador = "multi";
                        break;
                    case "/":
                        operar += Double.Parse(boxDisplay.Text);
                        boxDisplay.Clear();
                        operador = "divi";
                        break;
                    
                    case "CE":           
                        boxDisplay.Clear();
                        operador = "clear";
                        break;
                    case "=":
                        operaciones();
                        break;
                    default:

                        break;



                }

            }
        }
        private void operaciones()
        {
            if (operador.Equals("suma"))
            {
                operar += Double.Parse(boxDisplay.Text);
                boxDisplay.Text = "" + operar;
                operador = "";
                operar = 0.0;
            }
            if (operador.Equals("resta"))
            {
                operar = operar - Double.Parse(boxDisplay.Text);
                boxDisplay.Text = "" + operar;
                operador = "";
                operar = 0.0;
            }
            if (operador.Equals("multi"))
            {
                operar *= Double.Parse(boxDisplay.Text);
                boxDisplay.Text = "" + operar;
                operador = "";
                operar = 0.0;
            }
            if (operador.Equals("divi"))
            {
                operar /= Double.Parse(boxDisplay.Text);
                boxDisplay.Text = "" + operar;
                operador = "divi";
                operar = 0.0;
            }
           
            if (operador.Equals("clear"))
            {
                boxDisplay.Text = "";
                operador = "clear";
                operar = 0.0;

            }
        }


        private void buttonSuma_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);

        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void buttonMulti_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void buttonDivi_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        private void buttonDecimal_Click(object sender, EventArgs e)
           
        {

            boxDisplay.Text = boxDisplay.Text + ".";

        }

        private void buttonClean_Click(object sender, EventArgs e)
        {
            buttonIgual_Click(sender, e);
        }

        
    }
}